'use strict';

import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
const $ = gulpLoadPlugins();

// Concatenate, transpiles ES2015 code to ES5 and minify JavaScript.
gulp.task('scripts', () => {
	return gulp.src([
			'./src/js/vendors/*.js',
			'./src/js/main.js'
		])
		.pipe($.concat('main.min.js'))
		// .pipe($.babel())
		.pipe($.uglify())
		.pipe(gulp.dest('public/js'));
});

// Minify and add prefix to css.
gulp.task('css', () => {
	const AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];

	return gulp.src('public/css/main.css')
		.pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
		.pipe($.cssnano())
		.pipe(gulp.dest('public/css'));
});

// Compile scss to css.
gulp.task('scss', () => {
	return gulp.src('./src/scss/main.scss')
		.pipe($.sass({
			includePaths: ['css'],
			onError: browserSync.notify
		}))
		.pipe(gulp.dest('public/css'));
});

gulp.task('serve', gulp.series('scripts', 'scss', (done) => {
	browserSync.init({
		notify: false,
		proxy: 'http://localhost/kaes/'
	});

	gulp.watch('src/scss/**/*.scss', gulp.series('scss'));

	gulp.watch('src/js/**/*.js', gulp.series('scripts'));
	return done();
}));
