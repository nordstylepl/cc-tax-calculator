<?php

if ( !function_exists( 'tax_calculator_insert_db' ) ) {
    function tax_calculator_insert_db( $args = array() ) {
        global $wpdb;

        $defaults = array(
            'id'   => null,
            'date' => date( 'Y-m-d H:i:s' )
        );

        $args = wp_parse_args( $args, $defaults );
        $table_name = $wpdb->prefix . 'tax_calculator';

        if ( $wpdb->insert( $table_name, $args ) ) {
            return $wpdb->insert_id;
        }

        return false;

    }

}


if ( !function_exists( 'tax_calculator_send_ajax' ) ) {
    function tax_calculator_send_ajax() {

        $product_name = isset( $_POST[ 'product_name' ] ) ? esc_attr( sanitize_text_field( $_POST[ 'product_name' ] ) ) : false;
        $net_amount = isset( $_POST[ 'net_amount' ] ) ? absint( $_POST[ 'net_amount' ] ) : false;
        $currency = isset( $_POST[ 'currency' ] ) ? esc_attr( sanitize_text_field( $_POST[ 'currency' ] ) ) : false;
        $vat_rate = isset( $_POST[ 'vat_rate' ] ) ? absint( $_POST[ 'vat_rate' ] ) : false;

        $summary = isset( $_POST[ 'summary' ] ) ? absint( $_POST[ 'summary' ] ) : false;
        $vat_rate_value = isset( $_POST[ 'vat_rate_value' ] ) ? absint( $_POST[ 'vat_rate_value' ] ) : false;

        check_ajax_referer( 'form_nonce', 'nonce' );

        if ( !$product_name && !$net_amount && !$currency && !$vat_rate && !$vat_rate_value && !$summary ) {
            return;
        }

        ob_start();


        $fields = array(
            'product_name'   => $product_name,
            'net_amount'     => $net_amount,
            'currency'       => $currency,
            'vat_rate'       => $vat_rate,
            'summary'        => $summary,
            'vat_rate_value' => $vat_rate_value,
            'ip'             => tax_get_client_ip(),
            'date'           => date( 'Y-m-d H:i:s' )
        );

        tax_calculator_insert_db( $fields );

        $data = json_encode(
            array(
                "status"  => true,
                "message" => "Tax calculated saved"
            )
        );

        wp_send_json_success( $data );
        wp_die();

    }

    add_action( 'wp_ajax_tax_calculator_send_ajax', 'tax_calculator_send_ajax' );
    add_action( 'wp_ajax_nopriv_tax_calculator_send_ajax', 'tax_calculator_send_ajax' );

}


if ( !function_exists( 'tax_get_client_ip' ) ) {
    function tax_get_client_ip() {

        if ( getenv( 'HTTP_CLIENT_IP' ) )
            $ipaddress = getenv( 'HTTP_CLIENT_IP' );
        else if ( getenv( 'HTTP_X_FORWARDED_FOR' ) )
            $ipaddress = getenv( 'HTTP_X_FORWARDED_FOR' );
        else if ( getenv( 'HTTP_X_FORWARDED' ) )
            $ipaddress = getenv( 'HTTP_X_FORWARDED' );
        else if ( getenv( 'HTTP_FORWARDED_FOR' ) )
            $ipaddress = getenv( 'HTTP_FORWARDED_FOR' );
        else if ( getenv( 'HTTP_FORWARDED' ) )
            $ipaddress = getenv( 'HTTP_FORWARDED' );
        else if ( getenv( 'REMOTE_ADDR' ) )
            $ipaddress = getenv( 'REMOTE_ADDR' );
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}


if ( !function_exists( 'tax_calculator_shortcode' ) ) {
    function tax_calculator_shortcode() {

        if ( file_exists( CC_TEMPLATES . 'calculator-form.php' ) ) {

            $view = "";

            ob_start();
            require( CC_TEMPLATES . 'calculator-form.php' );
            $view .= ob_get_clean();

            return $view;
        }

        return false;
    }

    add_shortcode( 'tax-calculator', 'tax_calculator_shortcode' );
}