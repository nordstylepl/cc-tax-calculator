<?php

namespace CC\Core;

class Activate {

    public static function activate() {
        flush_rewrite_rules();

        global $wpdb;

        $table_name = $wpdb->prefix . 'tax_calculator';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
        id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,    
        product_name varchar(40) NOT NULL,
        net_amount varchar(40) NOT NULL,
        currency varchar(40) NOT NULL,
        vat_rate varchar(40) NOT NULL,
        vat_rate_value varchar(40) NOT NULL,
        summary varchar(40) NOT NULL,
        ip varchar(100) NOT NULL,
        date datetime not null
        ) $charset_collate;";

        if ( ! function_exists( 'maybe_create_table' ) ) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        }

        maybe_create_table( $table_name, $sql);

    }


}