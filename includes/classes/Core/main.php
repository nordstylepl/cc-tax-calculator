<?php

namespace CC\Core;
use CC;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 */


class Main {

    protected $loader;
    protected $plugin_name;
    protected $version;

    public function __construct() {

        if ( defined( 'CC_VERSION' ) ) {
            $this->version = CC_VERSION;
        } else {
            $this->version = '1.0.0';
        }

        $this->plugin_name = 'tax-calculator';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_public_hooks();

    }

    private function load_dependencies() {
        $this->loader = new CC\Core\Loader();
    }


    /**
     * Define the locale for this plugin for internationalization.
     */

    private function set_locale() {

        $plugin_i18n = new CC\Core\i18n();
        $plugin_i18n->load_plugin_textdomain();

    }



    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     */

    private function define_public_hooks() {
        $this->load_scripts();
        $this->load_styles();
    }


    public function load_scripts() {
        $args = array(
            'url'   => admin_url( 'admin-ajax.php' ),
            'price_product' => __('Product price','tax-calculator'),
            'currency' => __('zł','tax-calculator'),
            'is' => __('is','tax-calculator'),
            'net' => __('gross','tax-calculator'),
            'net_value' => __('tax amount is','tax-calculator'),

        );

        wp_enqueue_script( 'tax-calculator/js', CC_PLUGIN_PATH . 'public/js/main.min.js', [ 'jquery' ], $this->get_version(), true );
        wp_localize_script( 'tax-calculator/js', 'cc_settings', $args );
    }

    public function load_styles() {
        wp_enqueue_style( 'tax-calculator/css', CC_PLUGIN_PATH . 'public/css/main.css', [], $this->get_version() );
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     */

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() : string {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() : string {
        return $this->version;
    }

}