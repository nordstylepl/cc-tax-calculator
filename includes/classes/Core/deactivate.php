<?php

namespace CC\Core;

/**
 * Fired during plugin deactivation
 */

class Deactivate {

    public static function deactivate() {
        flush_rewrite_rules();
    }

}