<?php

namespace CC\Core;

/**
 * Define the internationalization functionality.
 */

class i18n {

    public function load_plugin_textdomain() {

        load_plugin_textdomain(
            'tax-calculator',
            false,
            basename( CC_PLUGIN_PATH ) . '/languages'
        );

    }

}