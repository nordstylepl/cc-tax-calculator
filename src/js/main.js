jQuery(document).ready(function ($) {

	if (typeof cc_settings !== 'undefined') {

		$(".tax-form").validate({

			submitHandler: function submitHandler(e) {

				var product_name = document.getElementById('product-name').value;
				var net_amount = document.getElementById('net-amount').value;
				var currency = document.getElementById('currency').value;
				var vat_rate = document.getElementById('var-rate').value;
				var nonce = document.getElementById('_wpnonce').value;

				var vat_rate_value = Number(net_amount * vat_rate / 100);
				var summary = Number(net_amount) + vat_rate_value;

				var data = {
					action: 'tax_calculator_send_ajax',
					product_name: product_name,
					net_amount: net_amount,
					currency: currency,
					vat_rate: vat_rate,
					nonce: nonce,
					summary: summary.toFixed(2),
					vat_rate_value: vat_rate_value,
				};

				$.post(cc_settings.url, data, function (response) {
					if (response.success) {
						tax_test(data);
					}
				}).fail(function (xhr, textStatus, e) {
					console.log(xhr.responseText);
				});

				return false;
			}

		});


		function tax_test(data) {

			var elm = $('.form-response');

			elm.fadeOut(100, function () {
				elm.html("<div class='product-name'>" + cc_settings.price_product + " <strong>" + data.product_name + "</strong> " + cc_settings.is + " <strong>" + data.summary + " " + cc_settings.currency + "</strong> " + cc_settings.net+ ", <br />" + cc_settings.net_value + " <strong>" + data.vat_rate_value + " " +cc_settings.currency +"</strong></div>");
				elm.fadeIn(400);
			});
		}
	}
});