<?php

use \CC\Core as Core;

/**
 * Plugin Name:    TAX-Calculator
 * Plugin URI:     http://coalacode.com/
 * Description:    Lightweight extension plugin. <strong>Shortcode to init [tax-calculator]</strong>
 * Author:         Ralph Lorenzo
 * Author URL:     http://coalacode.com/
 * Version:        1.0.0
 * Text Domain:    tax-calculator
 * Domain Path:    /languages
 */

require __DIR__ . '/vendor/autoload.php';

define( 'CC_VERSION', '1.0.0' );
define( 'CC_ABS_PATH', dirname( __FILE__ ) );
define( 'CC_REL_PATH', trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) );
define( 'CC_INC', CC_ABS_PATH . '/includes/' );
define( 'CC_TEMPLATES', CC_ABS_PATH . '/templates/' );
define( 'CC_PLUGIN_PATH', plugin_dir_url( __FILE__ ) );
define( 'CC_PLUGIN_PATH1', plugin_dir_path( __FILE__ ) );

function activate_cc() {
    Core\Activate::activate();
}

function deactivate_cc() {
    Core\Deactivate::deactivate();
}

register_activation_hook( __FILE__, 'activate_cc' );
register_deactivation_hook( __FILE__, 'deactivate_cc' );


/**
 * Begins execution of the plugin.
 */

if ( !function_exists( 'tax_calculator_init' ) ) {
    function tax_calculator_init() {

        $plugin = new Core\Main;
        $plugin->run();

        require_once CC_INC . 'functions.php';
    }

    add_action( "init", "tax_calculator_init" );
}