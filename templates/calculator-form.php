<div class="tax-calc">

    <h1 class="tax-calc__title">
        <?php _e( 'Calculate product tax', 'tax-calculator' ); ?>
    </h1>

    <form class="tax-form" method="post">

        <div class="tax-form__row">
            <div class="tax-form__inner flex flex-wrap">

                <div class="tax-form__group w-100">
                    <div class="tax-form__label">
                        <label for="product-name">
                            <?php _e( 'Product name', 'tax-calculator' ); ?>
                        </label>
                    </div>
                    <div class="tax-form__control">
                        <input type="text" name="product-name" id="product-name" class="w-100 tax-form__input" placeholder="<?php _e( 'Example: PEPSI', 'tax-calculator' ); ?>"
                               minlength="3" data-msg-required="<?php _e( 'Required field', 'tax-calculator' ); ?>" data-msg-minlength="<?php _e( 'Incorrect number of characters', 'tax-calculator' ); ?>" required="required"/>
                    </div>
                </div>

                <div class="tax-form__group w-100">
                    <div class="tax-form__label">
                        <label for="net-amount">
                            <?php _e( 'Net amount', 'tax-calculator' ); ?>
                        </label>
                    </div>
                    <div class="tax-form__control">
                        <input type="number" id="net-amount" name="net-amount" class="w-100 tax-form__input" placeholder="<?php _e( 'Example: 100 PLN', 'tax-calculator' ); ?>"
                               data-msg-required="<?php _e( 'Required field', 'tax-calculator' ); ?>" data-msg-number="<?php _e( 'Invalid characters', 'tax-calculator' ); ?>" required="required"/>
                    </div>
                </div>

                <div class="tax-form__group w-50">
                    <div class="tax-form__label">
                        <label for="currency">
                            <?php _e( 'Currency', 'tax-calculator' ); ?>
                        </label>
                    </div>
                    <div class="tax-form__control">
                        <input type="text" name="currency" id="currency" value="PLN" class="w-100 tax-form__input" required="required" disabled/>
                    </div>
                </div>

                <div class="tax-form__group w-50">
                    <div class="tax-form__label">
                        <label for="var-rate">
                            <?php _e( 'VAT rate', 'tax-calculator' ); ?>
                        </label>
                    </div>
                    <div class="tax-form__control">
                        <select name="var-rate" id="var-rate" class="w-100 tax-form__input" required="required">
                            <option value="23">23 %</option>
                            <option value="22">22 %</option>
                            <option value="8">8 %</option>
                            <option value="7">7 %</option>
                            <option value="5">5 %</option>
                            <option value="3">3 %</option>
                            <option value="0">0 %</option>
                        </select>
                    </div>
                </div>

                <div class="tax-form__group w-100">
                    <button class="tax-form__send m-button m-button--primary center mt5 form-send">
                        <?php _e( 'Calculate', 'tax-calculator' ); ?>
                    </button>
                </div>

                <?php wp_nonce_field( 'form_nonce' ); ?>

            </div>

        </div>
    </form>

    <div class="tax-calc__form-row">
        <div class="form-response tax-form__group w-100" style="display: none"></div>
    </div>

</div>